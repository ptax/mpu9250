'use strict';

class I2CBUS {

  constructor(options) {
    const bunyan = require('bunyan');
    this.logger = bunyan.createLogger({ name: 'i2c-bus' });
    this.logger.level((options && options.logLevel) ? options.logLevel : "error");
    this.logger.debug({ options: options }, 'I2CBUS');

    const i2c = require('i2c-bus');
    this.I2CBusNumber = (options && options.I2CBusNumber) ? options.I2CBusNumber : 1;
    this.bus = i2c.openSync(this.I2CBusNumber);
    if (this.bus) {
      this.logger.trace(`Bus i2c-${this.I2CBusNumber} opened`)
    } else {
      throw new Error(`Can\'t open bus i2c-${this.I2CBusNumber}`);
    }
  };

  static readBit(byte, pos) {
    return (byte >> pos) & 1;
  };

  readByteSync(device, register) {
    return this.bus.readByteSync(device, register);
  };

  writeByteSync(device, register, data) {
    this.bus.writeByteSync(device, register, data);
  };

  readI2cBlockSync(device, register, len, buffer) {
    return this.bus.readI2cBlockSync(device, register, len, buffer);
  };

  writeBits(device, register, pos, len, value) {
    if (!Number.isInteger(value) || value > 2 ** len - 1) {
      throw new Error('Writing more bits than space allocated!');
    }
    const byte = this.bus.readByteSync(device, register);
    this.printByte(byte);
    const mask = ((1 << len) - 1) << pos;
    const newByte = byte ^ ((byte ^ (value << pos)) & mask);
    this.printByte(newByte);
    this.bus.writeByteSync(device, register, newByte);
  };

  printByte(b) {
    this.logger.trace(`${I2CBUS.readBit(b, 7)} ${I2CBUS.readBit(b, 6)} ${I2CBUS.readBit(b, 5)} ${I2CBUS.readBit(b, 4)} ${I2CBUS.readBit(b, 3)} ${I2CBUS.readBit(b, 2)} ${I2CBUS.readBit(b, 1)} ${I2CBUS.readBit(b, 0)}`);
  };

}

module.exports = I2CBUS;
