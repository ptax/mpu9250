'use strict';

const sleep = require('sleep');
const bunyan = require('bunyan');
const _merge = require('lodash.merge');
const I2CBUS = require('./i2cBus');
const AK8963 = require('./ak8963');

class MPU9250 {

  constructor(options) {
    if (options.debug) { console.time('MPU9250 constructor') };
    this.i2cBus = null;

    this.I2C_ADDRESS_AD0_LOW   = 0x68;
    this.I2C_ADDRESS_AD0_HIGH  = 0x69;
    this.DEVICE_ID             = 0x71;

    // Registers
    this.RA_SELF_TEST_X_GYRO  = 0x00;
    this.RA_SELF_TEST_Y_GYRO  = 0x01;
    this.RA_SELF_TEST_Z_GYRO  = 0x02;
    this.RA_SELF_TEST_X_ACCEL = 0x0D;
    this.RA_SELF_TEST_Y_ACCEL = 0x0E;
    this.RA_SELF_TEST_Z_ACCEL = 0x0F;
    this.RA_XG_OFFSET_H       = 0x13;
    this.RA_XG_OFFSET_L       = 0x14;
    this.RA_YG_OFFSET_H       = 0x15;
    this.RA_YG_OFFSET_L       = 0x16;
    this.RA_ZG_OFFSET_H       = 0x17;
    this.RA_ZG_OFFSET_L       = 0x18;
    this.RA_SMPLRT_DIV        = 0x19;
    this.RA_CONFIG            = 0x1A;
    this.RA_GYRO_CONFIG       = 0x1B;
    this.RA_ACCEL_CONFIG_1    = 0x1C;
    this.RA_ACCEL_CONFIG_2    = 0x1D;
    this.RA_LP_ACCEL_ODR      = 0x1E;
    this.RA_WOM_THR           = 0x1F;
    this.RA_FIFO_EN           = 0x23;
    this.RA_I2C_MST_CTRL      = 0x24;
    this.RA_I2C_MST_STATUS    = 0x36;
    this.RA_INT_PIN_CFG       = 0x37;
    this.RA_INT_ENABLE        = 0x38;
    this.RA_INT_STATUS        = 0x3A;
    this.RA_ACCEL_XOUT_H      = 0x3B;
    this.RA_ACCEL_XOUT_L      = 0x3C;
    this.RA_ACCEL_YOUT_H      = 0x3D;
    this.RA_ACCEL_YOUT_L      = 0x3E;
    this.RA_ACCEL_ZOUT_H      = 0x3F;
    this.RA_ACCEL_ZOUT_L      = 0x40;
    this.RA_TEMP_OUT_H        = 0x41;
    this.RA_TEMP_OUT_L        = 0x42;
    this.RA_GYRO_XOUT_H       = 0x43;
    this.RA_GYRO_XOUT_L       = 0x44;
    this.RA_GYRO_YOUT_H       = 0x45;
    this.RA_GYRO_YOUT_L       = 0x46;
    this.RA_GYRO_ZOUT_H       = 0x47;
    this.RA_GYRO_ZOUT_L       = 0x48;
    this.RA_USER_CTRL         = 0x6A;
    this.RA_PWR_MGMT_1        = 0x6B;
    this.RA_PWR_MGMT_2        = 0x6C;
    this.RA_FIFO_COUNTH       = 0x72;
    this.RA_FIFO_COUNTL       = 0x73;
    this.RA_FIFO_R_W          = 0x74;
    this.RA_WHO_AM_I          = 0x75;
    this.RA_XA_OFFSET_H       = 0x77;
    this.RA_XA_OFFSET_L       = 0x78;
    this.RA_YA_OFFSET_H       = 0x7A;
    this.RA_YA_OFFSET_L       = 0x7B;
    this.RA_ZA_OFFSET_H       = 0x7D;
    this.RA_ZA_OFFSET_L       = 0x7E;

    // Field bits and lengths
    this.PWR1_CLKSEL          = { bit: 0, len: 3};
    this.PWR1_SLEEP           = { bit: 6, len: 1};
    this.PWR1_DEVICE_RESET    = { bit: 7, len: 1};
    this.GCONFIG_FSR_SEL      = { bit: 3, len: 2};
    this.ACONFIG_FSR_SEL      = { bit: 3, len: 2};
    this.USERCTRL_I2C_MST_EN  = { bit: 5, len: 1};
    this.INTCFG_BYPASS_EN     = { bit: 1, len: 1};

    // Values
    this.CLOCK_INTERNAL     = 0x00;
    this.CLOCK_PLL_XGYRO    = 0x01;   // Auto selects the best available clock source – PLL if ready, else use the Internal oscillator
    this.CLOCK_PLL_YGYRO    = 0x02;
    this.CLOCK_PLL_ZGYRO    = 0x03;
    this.CLOCK_KEEP_RESET   = 0x07;

    this.GFS_250DPS       = 0x00;
    this.GFS_500DPS       = 0x01;
    this.GFS_1000DPS      = 0x02;
    this.GFS_2000DPS      = 0x03;
    this.GSENSITIVITIES   = [131.0, 65.5, 32.8, 16.4];
    this.GRESOLUTIONS     = [250.0 / 32768.0, 500.0 / 32768.0, 1000.0 / 32768.0, 2000.0 / 32768.0];

    this.AFS_2G         = 0x00;
    this.AFS_4G         = 0x01;
    this.AFS_8G         = 0x02;
    this.AFS_16G        = 0x03;
    this.ASENSITIVITIES = [16384.0, 8192.0, 4096.0, 2048.0];
    this.ARESOLUTIONS   = [2.0 / 32768.0, 4.0 / 32768.0, 8.0 / 32768.0, 16.0 / 32768.0];

    this.config = {
      I2CBusNumber: 1,
      MPU9250_Address: this.I2C_ADDRESS_AD0_LOW,
      useMagnetometer: false,
      gyroMode: this.GFS_250DPS,
      accelMode: this.AFS_2G,
      magMode: 0x02,
      magResolution: 0x00,
      debug: false,
      logLevel: 'info'
    };
    _merge(this.config, options);

    // Setting sensitivity and resolution
    this.gRes = this.GRESOLUTIONS[this.config.gyroMode];
    this.aRes = this.ARESOLUTIONS[this.config.accelMode];
    this.gSens = this.GSENSITIVITIES[this.config.gyroMode];
    this.aSens = this.ASENSITIVITIES[this.config.accelMode];
    // Shortcut
    this.address = this.config.MPU9250_Address;

    // Configuring logger
    this.logger = bunyan.createLogger({ name: 'mpu9250' });
    this.logger.level(this.config.logLevel);
    this.logger.trace({ config: this.config }, 'mpu9250');
    console.log = this.logger.info.bind(this.logger);

    // Opening i2c bus
    this.i2cBus = new I2CBUS({
      I2CBusNumber: this.config.I2CBusNumber,
      logLevel: this.config.logLevel
    });
    // Verifying connection and device ID
    const deviceId = this.getDeviceID();
    if (deviceId !== this.DEVICE_ID) {
      throw new Error(`Unexpected MPU9250 device ID: 0x${deviceId.toString(16)}`);
    } else {
      this.logger.info(`Found MPU9250 device ID 0x${deviceId.toString(16)} on bus i2c-${this.config.I2CBusNumber}, address 0x${this.address.toString(16)}`);
    }

    // enable magnetometer
    this.setByPASSEnabled(true);
    this.setI2CMasterModeEnabled(false);
    if (this.config.useMagnetometer) {
      this.magnetometer = new AK8963(this.config);
    } else {
      this.magnetometer = null;
    }

    if (options.debug) { console.timeEnd('MPU9250 constructor') };
  };

  initialize(options) {
    this.logger.info('Initializing...');
    _merge(this.config, options);
    if (this.config.debug) { console.time('initialize') };
    // clear configuration (SLEEP, CYCLE, GYRO_STANDBY, PD_PTAT, CLKSEL[2:0])
    this.logger.debug('Clearing power settings on MPU9250...');
    this.i2cBus.writeByteSync(this.address, this.RA_PWR_MGMT_1, 0x00);
    // define clock source
    this.setClockSource(this.CLOCK_PLL_XGYRO);
    // configure Gyro, Thermometer, sample rate, DLPF
    this.prepareGyroAccelTherm();
    // define gyro range
    this.setGyroRange(this.config.gyroMode);
    // define accel range
    this.setAccelRange(this.config.accelMode);

    this.setI2CMasterModeEnabled(false);
    this.setByPASSEnabled(true);

    if (this.magnetometer) {
      this.logger.debug(`magMode ${this.config.magMode}`);
      this.logger.debug(`magResolution ${this.config.magResolution}`);
      this.magnetometer.initialize(options);
      this.magnetometer.calibrate();
    }
    if (this.config.debug) {
      this.printSettings();
      this.printAccelSettings();
      this.printGyroSettings();
      if (this.magnetometer) {
        this.magnetometer.printSettings();
      }
    }

    if (this.config.debug) { console.timeEnd('initialize') };
    return true;
  };

  calibrate() {
    this.logger.info('Calibrating...');
    if (this.config.debug) { console.time('calibrate') };

    this.reset();
    this.setGyroPowerSettings(0x00);
    this.setAccelPowerSettings(0x00);
    // Configure device for bias calculation
    // Disable all interrupts
    this.i2cBus.writeByteSync(this.address, this.RA_INT_ENABLE, 0x00);
    // Disable FIFO
    this.i2cBus.writeByteSync(this.address, this.RA_FIFO_EN, 0x00);
    this.i2cBus.writeBits(this.address, this.RA_USER_CTRL, 6, 1, 0x00);
    // Turn on internal clock source
    this.setClockSource(this.CLOCK_INTERNAL);
    // Disable I2C master
    this.setI2CMasterModeEnabled(false);
    this.i2cBus.writeByteSync(this.address, this.RA_I2C_MST_CTRL, 0x00);
    // Reset FIFO and DMP
    this.i2cBus.writeBits(this.address, this.RA_USER_CTRL, 2, 1, 0x01);
    sleep.msleep(20);

    // Set low-pass filter to 184 Hz
    this.i2cBus.writeBits(this.address, this.RA_CONFIG, 0, 2, 0x01);
    // Set sample rate to 1 kHz
    this.i2cBus.writeByteSync(this.address, this.RA_SMPLRT_DIV, 0x00);
    // Set gyro full-scale to 250 degrees per second, maximum sensitivity
    this.setGyroRange(this.GFS_250DPS);
    // Set accelerometer full-scale to 2 g, maximum sensitivity
    this.setAccelRange(this.AFS_2G);

    // Configure FIFO to capture accelerometer and gyro data for bias calculation
    this.i2cBus.writeBits(this.address, this.RA_USER_CTRL, 6, 1, 0x01);
    // Enable gyro and accelerometer sensors for FIFO  (max size 512 bytes in MPU-9150)
    this.i2cBus.writeByteSync(this.address, this.RA_FIFO_EN, 0x78);   // 0111 1000
    sleep.msleep(40);
    // At end of sample accumulation, turn off FIFO sensor read
    // Disable gyro and accelerometer sensors for FIFO
    this.i2cBus.writeByteSync(this.address, this.RA_FIFO_EN, 0x00);

    // Read FIFO sample count
    const fc_buf = Buffer.alloc(2);
    this.i2cBus.readI2cBlockSync(this.address, this.RA_FIFO_COUNTH, 2, fc_buf);
    const fifo_count = fc_buf.readInt16BE(0);
    // How many sets of full gyro and accelerometer data for averaging
    const packet_count = fifo_count/12;

    const gyro_bias = [0, 0, 0];
    const accel_bias = [0, 0, 0];
    const data = Buffer.alloc(12);
    for (var i = 0; i < packet_count; i++) {
      var accel_temp = [0, 0, 0]
      var gyro_temp = [0, 0, 0];
      // Read data for averaging
      this.i2cBus.readI2cBlockSync(this.address, this.RA_FIFO_R_W, 12, data);
      accel_temp[0] = data.readInt16BE(0);
      accel_temp[1] = data.readInt16BE(2);
      accel_temp[2] = data.readInt16BE(4);
      gyro_temp[0]  = data.readInt16BE(6);
      gyro_temp[1]  = data.readInt16BE(8);
      gyro_temp[2]  = data.readInt16BE(10);
      // Sum individual signed 16-bit biases to get accumulated signed 32-bit biases.
      accel_bias[0] += accel_temp[0];
      accel_bias[1] += accel_temp[1];
      accel_bias[2] += accel_temp[2];
      gyro_bias[0]  += gyro_temp[0];
      gyro_bias[1]  += gyro_temp[1];
      gyro_bias[2]  += gyro_temp[2];
    }
    for (var i = 0; i < 3; i++) {
      gyro_bias[i] /= packet_count;
      accel_bias[i] /= packet_count;
    }

    // remove gravity from the accel Z
    accel_bias[2] = (accel_bias[2] > 0) ? accel_bias[2] - this.aSens : accel_bias[2] + this.aSens;

    // Construct the gyro biases for push to the hardware gyro bias registers,
    // which are reset to zero upon device startup.
    // Divide by 4 to get 32.9 LSB per deg/s to conform to expected bias input format.
    // Biases are additive, so change sign on calculated average gyro biases
    for (var i = 0; i < 3; i++) {
      gyro_bias[i] = parseInt(gyro_bias[i] / 4);
    }
    data[0] = ( -gyro_bias[0] >> 8) & 0xFF;
    data[1] = ( -gyro_bias[0])      & 0xFF;
    data[2] = ( -gyro_bias[1] >> 8) & 0xFF;
    data[3] = ( -gyro_bias[1])      & 0xFF;
    data[4] = ( -gyro_bias[2] >> 8) & 0xFF;
    data[5] = ( -gyro_bias[2])      & 0xFF;

    // Push gyro biases to hardware registers
    this.i2cBus.writeByteSync(this.address, this.RA_XG_OFFSET_H, data[0]);
    this.i2cBus.writeByteSync(this.address, this.RA_XG_OFFSET_L, data[1]);
    this.i2cBus.writeByteSync(this.address, this.RA_YG_OFFSET_H, data[2]);
    this.i2cBus.writeByteSync(this.address, this.RA_YG_OFFSET_L, data[3]);
    this.i2cBus.writeByteSync(this.address, this.RA_ZG_OFFSET_H, data[4]);
    this.i2cBus.writeByteSync(this.address, this.RA_ZG_OFFSET_L, data[5]);

    // Output scaled gyro biases for display in the main program
    this.gyroBias = {
      x: gyro_bias[0] / this.gSens,
      y: gyro_bias[1] / this.gSens,
      z: gyro_bias[2] / this.gSens
    };

    // Construct the accelerometer biases for push to the hardware accelerometer
    // bias registers. These registers contain factory trim values which must be
    // added to the calculated accelerometer biases; on boot up these registers
    // will hold non-zero values. In addition, bit 0 of the lower byte must be
    // preserved since it is used for temperature compensation calculations.
    // Accelerometer bias registers expect bias input as 2048 LSB per g, so that
    // the accelerometer biases calculated above must be divided by 8.

    // A place to hold the factory accelerometer trim biases
    var accel_bias_reg = [0, 0, 0];
    // Read factory accelerometer trim values
    data.fill(0);
    this.i2cBus.readI2cBlockSync(this.address, this.RA_XA_OFFSET_H, 2, data);
    accel_bias_reg[0] = data.readInt16BE(0);
    this.i2cBus.readI2cBlockSync(this.address, this.RA_YA_OFFSET_H, 2, data);
    accel_bias_reg[1] = data.readInt16BE(0);
    this.i2cBus.readI2cBlockSync(this.address, this.RA_ZA_OFFSET_H, 2, data);
    accel_bias_reg[2] = data.readInt16BE(0);

    // Define mask for temperature compensation bit 0 of lower byte of accelerometer bias registers
    const accel_bias_reg_t = [accel_bias_reg[0] & 0x01, accel_bias_reg[1] & 0x01, accel_bias_reg[2] & 0x01];

    // Construct total accelerometer bias, including calculated average accelerometer bias from above
    // Subtract calculated averaged accelerometer bias scaled to 2048 LSB/g (16 g full scale)
    for (var i = 0; i < 3; i++) {
      accel_bias_reg[i] -= parseInt(accel_bias[i] / 8);
    }
    data[0] = ( accel_bias_reg[0] >> 8 ) & 0xFF;
    data[1] = ( accel_bias_reg[0] )      & 0xFE;
    data[2] = ( accel_bias_reg[1] >> 8 ) & 0xFF;
    data[3] = ( accel_bias_reg[1] )      & 0xFE;
    data[4] = ( accel_bias_reg[2] >> 8 ) & 0xFF;
    data[5] = ( accel_bias_reg[2] )      & 0xFE;

    // preserve temperature compensation bit when writing back to accelerometer bias registers
    data[1] = data[1] | accel_bias_reg_t[0];
    data[3] = data[3] | accel_bias_reg_t[1];
    data[5] = data[5] | accel_bias_reg_t[2];

    // Push accelerometer biases to hardware registers
    this.i2cBus.writeByteSync(this.address, this.RA_XA_OFFSET_H, data[0]);
    this.i2cBus.writeByteSync(this.address, this.RA_XA_OFFSET_L, data[1]);
    this.i2cBus.writeByteSync(this.address, this.RA_YA_OFFSET_H, data[2]);
    this.i2cBus.writeByteSync(this.address, this.RA_YA_OFFSET_L, data[3]);
    this.i2cBus.writeByteSync(this.address, this.RA_ZA_OFFSET_H, data[4]);
    this.i2cBus.writeByteSync(this.address, this.RA_ZA_OFFSET_L, data[5]);

    // Output scaled accelerometer biases for display in the main program
    this.accelBias = {
      x: accel_bias[0] / this.aSens,
      y: accel_bias[1] / this.aSens,
      z: accel_bias[2] / this.aSens
    };

    sleep.msleep(50);
    if (this.config.debug) { console.timeEnd('calibrate') };
  };

  selfTest() {
    if (this.config.debug) { console.time('self test') };
    this.logger.info('Testing...');
    // Set gyro sample rate to 1 kHz
    this.i2cBus.writeByteSync(this.address, this.RA_SMPLRT_DIV, 0x00);
    // Set gyro sample rate to 1 kHz and DLPF to 92 Hz
    this.i2cBus.writeBits(this.address, this.RA_CONFIG, 0, 2, 0x02);
    // Set full scale range for the gyro to 250 dps
    this.setGyroRange(this.GFS_250DPS);
    // Set accelerometer rate to 1 kHz and bandwidth to 92 Hz
    this.i2cBus.writeBits(this.address, this.RA_ACCEL_CONFIG_2, 0, 3, 0x02);
    // Set full scale range for the accelerometer to 2 g
    this.setAccelRange(this.AFS_2G);

    // Get average current values of gyro and acclerometer
    const gAvg = { x: 0, y: 0, z:0 };
    const aAvg = { x: 0, y: 0, z:0 };
    for (var i = 0; i < 200; i++) {
      var g = this.readRawGyroscope();
      gAvg.x += g.x;
      gAvg.y += g.y;
      gAvg.z += g.z;
      var a = this.readRawAccelerometer();
      aAvg.x += a.x;
      aAvg.y += a.y;
      aAvg.z += a.z;
    }
    gAvg.x /= 200;
    gAvg.y /= 200;
    gAvg.z /= 200;
    aAvg.x /= 200;
    aAvg.y /= 200;
    aAvg.z /= 200;

    // Enable self test on all three axes
    this.i2cBus.writeBits(this.address, this.RA_GYRO_CONFIG, 5, 3, 0b111);
    // Enable self test on all three axes
    this.i2cBus.writeBits(this.address, this.RA_ACCEL_CONFIG_1, 5, 3, 0b111);
    // Delay a while to let the device stabilize
    sleep.msleep(250);

    // Get average self-test values of gyro and acclerometer
    const gSTAvg = { x: 0, y: 0, z:0 };
    const aSTAvg = { x: 0, y: 0, z:0 };
    for (var i = 0; i < 200; i++) {
      var g = this.readRawGyroscope();
      gSTAvg.x += g.x;
      gSTAvg.y += g.y;
      gSTAvg.z += g.z;
      var a = this.readRawAccelerometer();
      aSTAvg.x += a.x;
      aSTAvg.y += a.y;
      aSTAvg.z += a.z;
    }
    gSTAvg.x /= 200;
    gSTAvg.y /= 200;
    gSTAvg.z /= 200;
    aSTAvg.x /= 200;
    aSTAvg.y /= 200;
    aSTAvg.z /= 200;

    // Configure the gyro and accelerometer for normal operation. NOTE: dirty
    this.i2cBus.writeByteSync(this.address, this.RA_GYRO_CONFIG, 0x00);
    this.i2cBus.writeByteSync(this.address, this.RA_ACCEL_CONFIG_1, 0x00);

    const sTest = {};
    // Get self-test results
    sTest.gx = this.i2cBus.readByteSync(this.address, this.RA_SELF_TEST_X_GYRO);
    sTest.gy = this.i2cBus.readByteSync(this.address, this.RA_SELF_TEST_Y_GYRO);
    sTest.gz = this.i2cBus.readByteSync(this.address, this.RA_SELF_TEST_Z_GYRO);
    sTest.ax = this.i2cBus.readByteSync(this.address, this.RA_SELF_TEST_X_ACCEL);
    sTest.ay = this.i2cBus.readByteSync(this.address, this.RA_SELF_TEST_Y_ACCEL);
    sTest.az = this.i2cBus.readByteSync(this.address, this.RA_SELF_TEST_Z_ACCEL);

    const FS = 0; // WTF is this? full scale mode?
    const fTrim = {};
    // Retrieve factory self-test value from self-test code reads
    fTrim.gx = (2620.0 / 1 << FS) * Math.pow(1.01, (sTest.gx - 1.0));
    fTrim.gy = (2620.0 / 1 << FS) * Math.pow(1.01, (sTest.gy - 1.0));
    fTrim.gz = (2620.0 / 1 << FS) * Math.pow(1.01, (sTest.gz - 1.0));
    fTrim.ax = (2620.0 / 1 << FS) * Math.pow(1.01, (sTest.ax - 1.0));
    fTrim.ay = (2620.0 / 1 << FS) * Math.pow(1.01, (sTest.ay - 1.0));
    fTrim.az = (2620.0 / 1 << FS) * Math.pow(1.01, (sTest.az - 1.0));

    if (this.config.debug) { console.timeEnd('self test') };
    // Report results as a ratio of (STR - FT)/FT
    return {
      gyroX: 100.0 * (gSTAvg.x - gAvg.x) / fTrim.gx - 100,
      gyroY: 100.0 * (gSTAvg.y - gAvg.y) / fTrim.gy - 100,
      gyroZ: 100.0 * (gSTAvg.z - gAvg.z) / fTrim.gz - 100,
      accelX: 100.0 * (aSTAvg.x - aAvg.x) / fTrim.ax - 100,
      accelY: 100.0 * (aSTAvg.y - aAvg.y) / fTrim.ay - 100,
      accelZ: 100.0 * (aSTAvg.z - aAvg.z) / fTrim.az - 100
    };
  };

  readRawGyroscope() {
    const buf = Buffer.alloc(6);
    this.i2cBus.readI2cBlockSync(this.address, this.RA_GYRO_XOUT_H, 6, buf);
    return {
      x: buf.readInt16BE(0),
      y: buf.readInt16BE(2),
      z: buf.readInt16BE(4)
    };
  };

  readRawAccelerometer() {
    const buf = Buffer.alloc(6);
    this.i2cBus.readI2cBlockSync(this.address, this.RA_ACCEL_XOUT_H, 6, buf);
    return {
      x: buf.readInt16BE(0),
      y: buf.readInt16BE(2),
      z: buf.readInt16BE(4)
    };
  };

  readRawThermometer() {
    const buf = Buffer.alloc(2);
    this.i2cBus.readI2cBlockSync(this.address, this.RA_TEMP_OUT_H, 2, buf);
    return {
      t: buf.readInt16BE(0)
    };
  };

  readRawMagnetometer() {
    if (this.magnetometer != null) {
      return this.magnetometer.readRawMagnetometer();
    } else {
      return [0, 0, 0];
    }
  };

  getTemperatureCelsius() {
    const tempRaw = this.readRawThermometer().t;
    return (tempRaw) ? ((tempRaw - 21.0) / 333.87) + 21.0 : 0;
  };

  getGyroPowerSettings() {
    if (this.i2cBus) {
      var byte = this.i2cBus.readByteSync(this.address, this.RA_PWR_MGMT_2);
      byte = byte & 0b00000111;
      return [
        (byte >> 2) & 1,    // X
        (byte >> 1) & 1,    // Y
        (byte >> 0) & 1     // Z
      ];
    }
    return false;
  };

  getAccelPowerSettings() {
    if (this.i2cBus) {
      var byte = this.i2cBus.readByteSync(this.address, this.RA_PWR_MGMT_2);
      byte = byte & 0b00111000;
      return [
        (byte >> 5) & 1,    // X
        (byte >> 4) & 1,    // Y
        (byte >> 3) & 1     // Z
      ];
    }
    return false;
  };

  getGyroRange() {
  	if (this.i2cBus) {
  		const byte = this.i2cBus.readByteSync(this.address, this.RA_GYRO_CONFIG);
  		return (byte >> 3) & 0b11;
  	}
  	return false;
  };

  getAccelRange() {
  	if (this.i2cBus) {
  		const byte = this.i2cBus.readByteSync(this.address, this.RA_ACCEL_CONFIG_1);
      return (byte >> 3) & 0b11;
  	}
  	return false;
  };

  getClockSource() {
    if (this.i2cBus) {
      const byte = this.i2cBus.readByteSync(this.address, this.RA_PWR_MGMT_1);
      return byte & 0b111; // read value of bits [2:0]
    }
    return false;
  };

  getI2CMasterMode() {
    if (this.i2cBus) {
      const byte = this.i2cBus.readByteSync(this.address, this.RA_USER_CTRL);
      return I2CBUS.readBit(byte, this.USERCTRL_I2C_MST_EN.bit);
    }
    return false;
  };

  getSleepEnabled() {
    if (this.i2cBus) {
      const byte = this.i2cBus.readByteSync(this.address, this.RA_PWR_MGMT_1);
      return I2CBUS.readBit(byte, this.PWR1_SLEEP.bit);
    }
    return false;
  };

  getByPASSEnabled() {
    if (this.i2cBus) {
      const byte = this.i2cBus.readByteSync(this.address, this.RA_INT_PIN_CFG);
      return I2CBUS.readBit(byte, this.INTCFG_BYPASS_EN.bit);
    }
    return false;
  };

  getDeviceID() {
    if (this.i2cBus) {
      return this.i2cBus.readByteSync(this.address, this.RA_WHO_AM_I);
    }
  };

  setByPASSEnabled(value) {
    this.logger.debug(`Setting bypass mode to ${value} ...`);
    this.i2cBus.writeBits(this.address, this.RA_INT_PIN_CFG, this.INTCFG_BYPASS_EN.bit, this.INTCFG_BYPASS_EN.len, value ? 0x01 : 0x00);
    sleep.msleep(10);
  };

  setI2CMasterModeEnabled(value) {
    this.logger.debug(`Setting I2C Master mode to ${value} ...`);
    this.i2cBus.writeBits(this.address, this.RA_USER_CTRL, this.USERCTRL_I2C_MST_EN.bit, this.USERCTRL_I2C_MST_EN.len, value ? 0x01 : 0x00);
    sleep.msleep(10);
  };

  setGyroPowerSettings(mask) {
    this.logger.debug(`Setting gyroscope power mode to 0b${mask.toString(2)} ...`);
    if (!Number.isInteger(mask) || mask < 0 || mask > 7) {
      throw new Error('Gyro power settings mask must be an int between [0,7]');
    }
    this.i2cBus.writeBits(this.address, this.RA_PWR_MGMT_2, 0, 2, mask);
    sleep.msleep(10);
  };

  setAccelPowerSettings(mask) {
    this.logger.debug(`Setting accelerometer power mode to 0b${mask.toString(2)} ...`);
    if (!Number.isInteger(mask) || mask < 0 || mask > 7) {
      throw new Error('Accelerometer power settings mask must be an int between [0,7]');
    }
    this.i2cBus.writeBits(this.address, this.RA_PWR_MGMT_2, 3, 2, mask);
    sleep.msleep(10);
  };

  setGyroRange(value) {
    this.logger.debug(`Setting gyroscope range to 0x${value.toString(16)} ...`);
    if (!Number.isInteger(value) || value < 0 || value > 3) {
      throw new Error('Gyroscope range value must be an int between [0,3]');
    }
    // Clear Fchoice bits [1:0]
    this.i2cBus.writeBits(this.address, this.RA_GYRO_CONFIG, 0, 2, 0x00);
    this.i2cBus.writeBits(this.address, this.RA_GYRO_CONFIG, this.GCONFIG_FSR_SEL.bit, this.GCONFIG_FSR_SEL.len, value);
    sleep.msleep(10);
    this.gRes = this.GRESOLUTIONS[value];
    this.gSens = this.GSENSITIVITIES[value];
  };

  setAccelRange(value) {
    this.logger.debug(`Setting accelerometer range to 0x${value.toString(16)} ...`);
    if (!Number.isInteger(value) || value < 0 || value > 3) {
      throw new Error('Accelerometer range value must be an int between [0,3]');
    }
    this.i2cBus.writeBits(this.address, this.RA_ACCEL_CONFIG_1, this.ACONFIG_FSR_SEL.bit, this.ACONFIG_FSR_SEL.len, value);
    sleep.msleep(10);
    this.aRes = this.ARESOLUTIONS[value];
    this.aSens = this.ASENSITIVITIES[value];
  };

  setClockSource(value) {
    this.logger.debug(`Setting clock to 0x${value.toString(16)} ...`);
    if (!Number.isInteger(value) || value < 0 || value > 7) {
      throw new Error('Clock source value must be an int between [0,7]');
    }
    this.i2cBus.writeBits(this.address, this.RA_PWR_MGMT_1, this.PWR1_CLKSEL.bit, this.PWR1_CLKSEL.len, value);
    sleep.msleep(10);
  };

  setSleepEnabled(value) {
    this.logger.debug(`Setting sleep mode to ${value} ...`);
    this.i2cBus.writeBits(this.address, this.RA_PWR_MGMT_1, this.PWR1_SLEEP.bit, this.PWR1_SLEEP.len, value ? 0x01 : 0x00);
    sleep.msleep(10);
  };

  // Configure Gyro and Thermometer
  // Disable FSYNC, DLPF_CFG = bits 2:0 = 011;
  // Set sample rate = gyroscope output rate/(1 + SMPLRT_DIV)
  // Clear accel_fchoice_b (bit 3) and set A_DLPFG (bits [2:0])
  prepareGyroAccelTherm() {
    this.logger.debug('Setting sample rate and DLPF ...');
    // Configure Gyro and Thermometer
    // Disable FSYNC and set thermometer and gyro bandwidth to 41 and 42 Hz, respectively;
    // minimum delay time for this setting is 5.9 ms, which means sensor fusion
    // update rates cannot be higher than 1 / 0.0059 = 170 Hz
    // DLPF_CFG = bits 2:0 = 011; this limits the sample rate to 1000 Hz for both
    // With the MPU9250, it is possible to get gyro sample rates of 32 kHz (!), 8 kHz, or 1 kHz
    this.i2cBus.writeBits(this.address, this.RA_CONFIG, 0, 6, 0b11);
    // Set sample rate = gyroscope output rate/(1 + SMPLRT_DIV)
    // Use a 200 Hz rate; a rate consistent with the filter update rate determined inset in CONFIG above.
    this.i2cBus.writeByteSync(this.address, this.RA_SMPLRT_DIV, 0x04);
    // Clear accel_fchoice_b (bit 3) and A_DLPFG (bits [2:0])
    // Set accelerometer rate to 1 kHz and bandwidth to 41 Hz
    this.i2cBus.writeBits(this.address, this.RA_ACCEL_CONFIG_2, 0, 4, 0b011);
    sleep.msleep(10);
  };

  reset() {
    this.logger.debug('Resetting configuration on MPU9250...');
    this.i2cBus.writeBits(this.address, this.RA_PWR_MGMT_1, this.PWR1_DEVICE_RESET.bit, this.PWR1_DEVICE_RESET.len, 0x01);
    sleep.msleep(100);
  };

  printSettings() {
    const CLK_RNG = [
      '0 (Internal 20MHz oscillator)',
      '1 (Auto selects the best available clock source)',
      '2 (Auto selects the best available clock source)',
      '3 (Auto selects the best available clock source)',
      '4 (Auto selects the best available clock source)',
      '5 (Auto selects the best available clock source)',
      '6 (Internal 20MHz oscillator)',
      '7 (Stops the clock and keeps timing generator in reset)'
    ];
    this.logger.info(`INFO MPU9250:`);
    this.logger.info(`INFO --> Device address: 0x${this.address.toString(16)}`);
    this.logger.info(`INFO --> i2c bus: ${this.config.I2CBusNumber}`);
    this.logger.info(`INFO --> Device ID: 0x${this.getDeviceID().toString(16)}`);
    this.logger.info(`INFO --> BYPASS enabled: ${(this.getByPASSEnabled() ? 'Yes' : 'No')}`);
    this.logger.info(`INFO --> SleepEnabled Mode: ${(this.getSleepEnabled() === 1 ? 'On' : 'Off')}`);
    this.logger.info(`INFO --> i2c Master Mode: ${(this.getI2CMasterMode() === 1 ? 'Enabled' : 'Disabled')}`);
    this.logger.info(`INFO --> Power Management (0x6B, 0x6C):`);
    this.logger.info(`INFO   --> Clock Source: ${CLK_RNG[this.getClockSource()]}`);
    this.logger.info(`INFO   --> Accel enabled (x, y, z): ${this.vectorToYesNo(this.getAccelPowerSettings())}`);
    this.logger.info(`INFO   --> Gyro enabled (x, y, z): ${this.vectorToYesNo(this.getGyroPowerSettings())}`);
  };

  vectorToYesNo(v) {
    var str = '(';
    str += v[0] ? 'No, ' : 'Yes, ';
    str += v[1] ? 'No, ' : 'Yes, ';
    str += v[2] ? 'No' : 'Yes';
    str += ')';
    return str;
  };

  printGyroSettings() {
    const FS_RANGE = ['+250dps (0)', '+500 dps (1)', '+1000 dps (2)', '+2000 dps (3)'];
  	this.logger.info(`INFO Gyroscope:`);
    this.logger.info(`INFO --> Full Scale Range (0x1B): ${FS_RANGE[this.getGyroRange()]}`);
  };

  printAccelSettings() {
    const FS_RANGE = [ '±2g (0)', '±4g (1)', '±8g (2)', '±16g (3)' ];
  	this.logger.info(`INFO Accelerometer:`);
  	this.logger.info(`INFO --> Full Scale Range (0x1C): ${FS_RANGE[this.getAccelRange()]}`);
  };

}

module.exports = MPU9250;
