'use strict';
const sleep = require('sleep');

const MPU9250 = require('./mpu9250');
const mpu9250 = new MPU9250({
  debug: false,
  logLevel: 'debug',
  useMagnetometer: true
});

// const Q = require('./quaternionFilters.js');
// const q = new Q();

// mpu9250.logger.warn({ Values: mpu9250.selfTest() }, 'Accelerometer and Gyroscope trim within, %:');

mpu9250.calibrate();
mpu9250.logger.info({
  gyroBias: {
    x: (mpu9250.gyroBias.x).toFixed(4) + ' o/s',
    y: (mpu9250.gyroBias.y).toFixed(4) + ' o/s',
    z: (mpu9250.gyroBias.z).toFixed(4) + ' o/s'
  },
  accelBias: {
    x: (mpu9250.accelBias.x * 1000).toFixed(4) + ' mg',
    y: (mpu9250.accelBias.y * 1000).toFixed(4) + ' mg',
    z: (mpu9250.accelBias.z * 1000).toFixed(4) + ' mg',
  }
}, 'Calibration of MPU9250 complete');

mpu9250.initialize({
  gyroMode: mpu9250.GFS_1000DPS,
  accelMode: mpu9250.AFS_8G,
  magMode: mpu9250.magnetometer.CNTL_MODE_CONT_100HZ,
  magResolution: mpu9250.magnetometer.MFS_14BITS
});
mpu9250.logger.debug({
  calibration: mpu9250.magnetometer.calibration,
  mRes: mpu9250.magnetometer.mRes,
}, 'Magnetometer calibration data (mG)');

mpu9250.logger.debug({ Temperature: mpu9250.getTemperatureCelsius() }, 'MPU9250 getTemperatureCelsius');


var AHRS = require('ahrs');
var madgwick = new AHRS({

    /*
     * The sample interval, in Hz.
     */
    // sampleInterval: 73,

    /*
     * Choose from the `Madgwick` or `Mahony` filter.
     */
    algorithm: 'Madgwick',

    /*
     * The filter noise value, smaller values have
     * smoother estimates, but have higher latency.
     * This only works for the `Madgwick` filter.
     */
    beta: 0.04,

    /*
     * The filter noise values for the `Mahony` filter.
     */
    kp: 0.5,
    ki: 0
});

let c = 0;
let t = Date.now();
let dt = 0;
function loop() {
  if (mpu9250.magnetometer.getDataReady()) {
    c++;
    // let gyroPoint = mpu9250.readRawGyroscope();
    // let accelPoint = mpu9250.readRawAccelerometer();
    let magPoint = mpu9250.readRawMagnetometer();
/*
    q.MahonyQuaternionUpdate(
      accelPoint.x * mpu9250.aRes,
      accelPoint.y * mpu9250.aRes,
      accelPoint.z * mpu9250.aRes,
      gyroPoint.x * mpu9250.gRes,
      gyroPoint.y * mpu9250.gRes,
      gyroPoint.z * mpu9250.gRes,
      magPoint[0] * mpu9250.magnetometer.mRes * mpu9250.magnetometer.asax - mpu9250.magnetometer.calibration.bias.x,
      magPoint[1] * mpu9250.magnetometer.mRes * mpu9250.magnetometer.asay - mpu9250.magnetometer.calibration.bias.y,
      magPoint[2] * mpu9250.magnetometer.mRes * mpu9250.magnetometer.asaz - mpu9250.magnetometer.calibration.bias.z
    );
*/
    // dt = (Date.now() - t) / 1000;
    // madgwick.update(
    //   gyroPoint.x * mpu9250.gRes * Math.PI / 180,
    //   gyroPoint.y * mpu9250.gRes * Math.PI / 180,
    //   gyroPoint.z * mpu9250.gRes * Math.PI / 180,
    //   accelPoint.x * mpu9250.aRes,
    //   accelPoint.y * mpu9250.aRes,
    //   accelPoint.z * mpu9250.aRes,
    //   magPoint[0] * mpu9250.magnetometer.mRes * mpu9250.magnetometer.asax, // - mpu9250.magnetometer.calibration.bias.x,
    //   magPoint[1] * mpu9250.magnetometer.mRes * mpu9250.magnetometer.asay, // - mpu9250.magnetometer.calibration.bias.y,
    //   magPoint[2] * mpu9250.magnetometer.mRes * mpu9250.magnetometer.asaz, // - mpu9250.magnetometer.calibration.bias.z,
    //   dt
    // );
    // t = Date.now();

    // if (c % 20 == 0) {
      // mpu9250.logger.warn(`y: ${q.yaw.toFixed(4)}    \tp: ${q.pitch.toFixed(4)}\tr: ${q.roll.toFixed(4)} \t${q.deltat}`);
      // mpu9250.logger.warn(`x: ${(accelPoint.x * mpu9250.aRes).toFixed(4)}    \ty: ${(accelPoint.y * mpu9250.aRes).toFixed(4)}\tz: ${(accelPoint.z * mpu9250.aRes).toFixed(4)} \t${q.deltat}`);
      // mpu9250.logger.warn(`x: ${(gyroPoint.x * mpu9250.gRes).toFixed(4)}    \ty: ${(gyroPoint.y * mpu9250.gRes).toFixed(4)}\tz: ${(gyroPoint.z * mpu9250.gRes).toFixed(4)} \t${q.deltat}`);
      // var res = madgwick.getEulerAngles();
      // mpu9250.logger.warn(`h: ${(res.heading * 180 / Math.PI).toFixed(4)}\t p: ${(res.pitch * 180 / Math.PI).toFixed(4)}\t r: ${(res.roll * 180 / Math.PI).toFixed(4)}\t dt: ${dt}`);
      // c = 0;
      var heading = Math.atan2(
        magPoint[1] * mpu9250.magnetometer.mRes * mpu9250.magnetometer.asay - mpu9250.magnetometer.calibration.bias.y,
        magPoint[0] * mpu9250.magnetometer.mRes * mpu9250.magnetometer.asax - mpu9250.magnetometer.calibration.bias.x
      );
      if (heading < 0) {
        heading += 2 * Math.PI;
      }
      heading *= 180 / Math.PI;
      mpu9250.logger.warn(heading);
    // }
  } else {
    mpu9250.logger.warn('Magnetometer not ready');
  }
}

setInterval(loop, 100);
