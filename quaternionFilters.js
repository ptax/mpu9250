'use strict';

const microtime = require('microtime');

class Q {

  constructor() {
    // These are the free parameters in the Mahony filter and fusion scheme, Kp
    // for proportional feedback, Ki for integral
    this.Kp = 2.0 * 5.0;
    this.Ki = 0.0;
    // Vector to hold integral error for Mahony method
    this.eInt = [0.0, 0.0, 0.0];
    // Vector to hold quaternion
    this.q = [1.0, 0.0, 0.0, 0.0];

    this.timeNow = microtime.now();
    this.deltat = 0;
    this.timeLastUpdate = this.timeNow;
  };

  // Set integration time by time elapsed since last filter update
  updateTime() {
    this.timeNow = microtime.now();
    this.deltat = ((this.timeNow - this.timeLastUpdate) / 1000000.0);
    this.timeLastUpdate = this.timeNow;
  }

  MahonyQuaternionUpdate(ax, ay, az, gx, gy, gz, mx, my, mz) {
    this.updateTime();
    // short name local variable for readability
    let q1 = this.q[0], q2 = this.q[1], q3 = this.q[2], q4 = this.q[3];
    let Kp = this.Kp, Ki = this.Ki;
    let deltat = this.deltat;
    let norm;
    let hx, hy, bx, bz;
    let vx, vy, vz, wx, wy, wz;
    let ex, ey, ez;
    let pa, pb, pc;

    // Auxiliary variables to avoid repeated arithmetic
    let q1q1 = q1 * q1;
    let q1q2 = q1 * q2;
    let q1q3 = q1 * q3;
    let q1q4 = q1 * q4;
    let q2q2 = q2 * q2;
    let q2q3 = q2 * q3;
    let q2q4 = q2 * q4;
    let q3q3 = q3 * q3;
    let q3q4 = q3 * q4;
    let q4q4 = q4 * q4;

    // Normalise accelerometer measurement
    norm = Math.sqrt(ax * ax + ay * ay + az * az);
    if (norm == 0.0) return; // Handle NaN
    norm = 1.0 / norm;       // Use reciprocal for division
    ax *= norm;
    ay *= norm;
    az *= norm;

    // Normalise magnetometer measurement
    norm = Math.sqrt(mx * mx + my * my + mz * mz);
    if (norm == 0.0) return; // Handle NaN
    norm = 1.0 / norm;       // Use reciprocal for division
    mx *= norm;
    my *= norm;
    mz *= norm;

    // Reference direction of Earth's magnetic field
    hx = 2.0 * mx * (0.5 - q3q3 - q4q4) + 2.0 * my * (q2q3 - q1q4) + 2.0 * mz * (q2q4 + q1q3);
    hy = 2.0 * mx * (q2q3 + q1q4) + 2.0 * my * (0.5 - q2q2 - q4q4) + 2.0 * mz * (q3q4 - q1q2);
    bx = Math.sqrt((hx * hx) + (hy * hy));
    bz = 2.0 * mx * (q2q4 - q1q3) + 2.0 * my * (q3q4 + q1q2) + 2.0 * mz * (0.5 - q2q2 - q3q3);

    // Estimated direction of gravity and magnetic field
    vx = 2.0 * (q2q4 - q1q3);
    vy = 2.0 * (q1q2 + q3q4);
    vz = q1q1 - q2q2 - q3q3 + q4q4;
    wx = 2.0 * bx * (0.5 - q3q3 - q4q4) + 2.0 * bz * (q2q4 - q1q3);
    wy = 2.0 * bx * (q2q3 - q1q4) + 2.0 * bz * (q1q2 + q3q4);
    wz = 2.0 * bx * (q1q3 + q2q4) + 2.0 * bz * (0.5 - q2q2 - q3q3);

    // Error is cross product between estimated direction and measured direction of gravity
    ex = (ay * vz - az * vy) + (my * wz - mz * wy);
    ey = (az * vx - ax * vz) + (mz * wx - mx * wz);
    ez = (ax * vy - ay * vx) + (mx * wy - my * wx);
    if (Ki > 0.0) {
      this.eInt[0] += ex;      // accumulate integral error
      this.eInt[1] += ey;
      this.eInt[2] += ez;
    } else {
      this.eInt[0] = 0.0;     // prevent integral wind up
      this.eInt[1] = 0.0;
      this.eInt[2] = 0.0;
    }

    // Apply feedback terms
    gx = gx + Kp * ex + Ki * this.eInt[0];
    gy = gy + Kp * ey + Ki * this.eInt[1];
    gz = gz + Kp * ez + Ki * this.eInt[2];

    // Integrate rate of change of quaternion
    pa = q2;
    pb = q3;
    pc = q4;
    q1 = q1 + (-q2 * gx - q3 * gy - q4 * gz) * (0.5 * deltat);
    q2 = pa + (q1 * gx + pb * gz - pc * gy) * (0.5 * deltat);
    q3 = pb + (q1 * gy - pa * gz + pc * gx) * (0.5 * deltat);
    q4 = pc + (q1 * gz + pa * gy - pb * gx) * (0.5 * deltat);

    // Normalise quaternion
    norm = 1.0 / Math.sqrt(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);
    // q1 *= norm;
    // q2 *= norm;
    // q3 *= norm;
    // q4 *= norm;
    this.q[0] = q1 * norm;
    this.q[1] = q2 * norm;
    this.q[2] = q3 * norm;
    this.q[3] = q4 * norm;

    this.yaw   = Math.atan2(2.0 * (this.q[1] * this.q[2] + this.q[0] * this.q[3]), this.q[0] * this.q[0] + this.q[1] * this.q[1] - this.q[2] * this.q[2] - this.q[3] * this.q[3]);
    this.pitch = -Math.asin(2.0 * (this.q[1] * this.q[3] - this.q[0] * this.q[2]));
    this.roll  = Math.atan2(2.0 * (this.q[0] * this.q[1] + this.q[2] * this.q[3]), this.q[0] * this.q[0] - this.q[1] * this.q[1] - this.q[2] * this.q[2] + this.q[3] * this.q[3]);
    this.pitch *= 180.0 / Math.PI;
    this.yaw   *= 180.0 / Math.PI;
    this.roll  *= 180.0 / Math.PI;

    // const RAD_TO_DEG = 57.295779513082320876798154814105;
    // this.yaw = Math.atan2(2.0 * (q2 * q3 + q1 * q4), q1 * q1 + q2 * q2 - q3 * q3 - q4 * q4);
    // this.pitch = -Math.asin(2.0 * (q2 * q4 - q1 * q3));
    // this.roll  = Math.atan2(2.0 * (q1 * q2 + q3 * q4), q1 * q1 - q2 * q2 - q3 * q3 + q4 * q4);
    // this.pitch *= RAD_TO_DEG;
    // this.yaw   *= RAD_TO_DEG;
    //
    // // Declination of SparkFun Electronics (40°05'26.6"N 105°11'05.9"W) is
    // // 	8° 30' E  ± 0° 21' (or 8.5°) on 2016-07-19
    // // - http://www.ngdc.noaa.gov/geomag-web/#declination
    // myIMU.yaw  -= 8.5;
    // this.roll *= RAD_TO_DEG;
  };
}

module.exports = Q;
