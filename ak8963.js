'use strict';

const sleep = require('sleep');
const bunyan = require('bunyan');
const _merge = require('lodash.merge');
const I2CBUS = require('./i2cBus');

class AK8963 {

  constructor(options) {
    if (options.debug) { console.time('AK8963 constructor') };
    this.i2cBus = null;

    this.ADDRESS    = 0x0C;
    this.DEVICE_ID  = 0x48;

    // Registers
    this.RA_WHO_AM_I   = 0x00;   // should return 0x48,
    this.RA_INFO       = 0x01;
    this.RA_ST1        = 0x02;   // data ready status bit 0
    this.RA_XOUT_L     = 0x03;   // data
    this.RA_XOUT_H     = 0x04;
    this.RA_YOUT_L     = 0x05;
    this.RA_YOUT_H     = 0x06;
    this.RA_ZOUT_L     = 0x07;
    this.RA_ZOUT_H     = 0x08;
    this.RA_ST2        = 0x09;   // Data overflow bit 3 and data read error status bit 2
    this.RA_CNTL       = 0x0A;   // Power down (0000), single-measurement (0001), self-test (1000) and Fuse ROM (1111) modes on bits 3:0
    this.RA_CNTL2      = 0x0B;
    this.RA_ASTC       = 0x0C;   // Self test control
    this.RA_I2CDIS     = 0x0F;   // I2C disable
    this.RA_ASAX       = 0x10;   // Fuse ROM x-axis sensitivity adjustment value
    this.RA_ASAY       = 0x11;   // Fuse ROM y-axis sensitivity adjustment value
    this.RA_ASAZ       = 0x12;

    // Bits
    this.ST1_DRDY_BIT = 0;
    this.ST1_DOR_BIT  = 1;
    this.ST2_HOFL_BIT = 3;

    this.CNTL_MODE_OFF                = 0x00; // Power-down mode
    this.CNTL_MODE_SINGLE_MEASURE     = 0x01; // Single measurement mode
    this.CNTL_MODE_CONT_8HZ           = 0x02; // Continuous measurement mode 1 - Sensor is measured periodically at 8Hz
    this.CNTL_MODE_CONT_100HZ         = 0x06; // Continuous measurement mode 2 - Sensor is measured periodically at 100Hz
    this.CNTL_MODE_EXT_TRIG_MEASURE   = 0x04; // External trigger measurement mode
    this.CNTL_MODE_SELF_TEST_MODE     = 0x08; // Self-test mode
    this.CNTL_MODE_FUSE_ROM_ACCESS    = 0x0F; // Fuse ROM access mode

    this.MFS_14BITS         = 0x00;
    this.MFS_16BITS         = 0x01;
    this.MRESOLUTIONS       = [6, 1.5];

    this.config = {
      i2cMagAddress: this.ADDRESS,
      magMode: this.CNTL_MODE_CONT_8HZ,
      magResolution: this.MFS_14BITS
    };
    this.calibration = {
       "bias": {
         "x": 593.9720219017094,
         "y": 585.2802483974359,
         "z": -699.5354853479853
       },
       "scale": {
         "x": 0.888,
         "y": 0.9310344827586207,
         "z": 1.08
       }
     }
     _merge(this.config, options);

    // Setting resolution
    this.mRes = this.MRESOLUTIONS[this.config.magResolution];
    // Shortcut
    this.address = this.config.i2cMagAddress;

    this.logger = bunyan.createLogger({ name: 'ak8963' });
    this.logger.level(this.config.logLevel);
    this.logger.trace({ config: this.config }, 'ak8963');
    console.log = this.logger.info.bind(this.logger);

    // Opening i2c bus
    this.i2cBus = new I2CBUS({
      I2CBusNumber: this.config.I2CBusNumber,
      logLevel: this.config.logLevel
    });
    // Verifying connection and device ID
  	const deviceId = this.getDeviceID();
    if (deviceId !== this.DEVICE_ID) {
      throw new Error(`Unexpected AK8963 device ID: 0x${deviceId.toString(16)}`);
    } else {
      this.logger.info(`Found AK8963 device ID 0x${deviceId.toString(16)} on bus i2c-${this.config.I2CBusNumber}, address 0x${this.address.toString(16)}`);
    }

    if (options.debug) { console.timeEnd('AK8963 constructor') };
  };

  initialize(options) {
    this.logger.info('Initializing...');
    _merge(this.config, options);
    if (options.debug) { console.time('AK8963 initialize') };
    // TODO: reset and setup
		this.getFactoryCalibration();
		this.setMode(this.config.magMode);
    this.setResolution(this.config.magResolution);
    if (options.debug) { console.timeEnd('AK8963 initialize') };
  };

  calibrate() {
    this.logger.info(`Calibrating... Mag res = ${this.mRes}`);
    if (this.config.debug) { console.time('AK8963 calibrate') };
    const mMode = this.getMode();
    var sample_count = 0;
    if (mMode == this.CNTL_MODE_CONT_8HZ) {
      sample_count = 128;
    } else if (mMode == this.CNTL_MODE_CONT_100HZ) {
      sample_count = 1500;
    } else {
      throw new Error('Magnetometer must be in CNTL_MODE_CONT_8HZ or CNTL_MODE_CONT_100HZ mode to calibrate');
    }

    // this.logger.warn(`Mag Calibration: Wave device in a figure 8 until done!\n4 seconds to get ready followed by 15 seconds of sampling`);
    // sleep.msleep(4000);
    const mag_bias = [0, 0, 0];
    const mag_scale = [0, 0, 0];
/*
    const mag_max = [Number.MIN_SAFE_INTEGER, Number.MIN_SAFE_INTEGER, Number.MIN_SAFE_INTEGER];
    const mag_min = [Number.MAX_SAFE_INTEGER, Number.MAX_SAFE_INTEGER, Number.MAX_SAFE_INTEGER];
    for (var i = 0; i < sample_count; i++) {
      // Read the mag data
      var mag_temp = this.readRawMagnetometer();
      for (var j = 0; j < 3; j++) {
        if (mag_temp[j] > mag_max[j]) {
          mag_max[j] = mag_temp[j];
        }
        if (mag_temp[j] < mag_min[j]) {
          mag_min[j] = mag_temp[j];
        }
      }
      if (mMode == this.CNTL_MODE_CONT_8HZ) {
        sleep.msleep(135);  // At 8 Hz ODR, new mag data is available every 125 ms
      }
      if (mMode == this.CNTL_MODE_CONT_100HZ) {
        sleep.msleep(12);   // At 100 Hz ODR, new mag data is available every 10 ms
      }
    }
*/
    let mag_max = [628, 546, 303];
    let mag_min = [-104, -244, -522];
    // Get hard iron correction
    // Get 'average' x mag bias in counts
    mag_bias[0] = (mag_max[0] + mag_min[0]) / 2;
    // Get 'average' y mag bias in counts
    mag_bias[1] = (mag_max[1] + mag_min[1]) / 2;
    // Get 'average' z mag bias in counts
    mag_bias[2] = (mag_max[2] + mag_min[2]) / 2;
    // Get soft iron correction estimate
    // Get average x axis max chord length in counts
    mag_scale[0] = (mag_max[0] - mag_min[0]) / 2;
    // Get average y axis max chord length in counts
    mag_scale[1] = (mag_max[1] - mag_min[1]) / 2;
    // Get average z axis max chord length in counts
    mag_scale[2] = (mag_max[2] - mag_min[2]) / 2;
    const avg_rad = (mag_scale[0] + mag_scale[1] + mag_scale[2]) / 3.0;

    this.logger.info(`Magnetometer calibrated`);
    if (this.config.debug) { console.timeEnd('AK8963 calibrate') };
    this.calibration = {
      bias: {
        x: mag_bias[0] * this.mRes * this.asax,
        y: mag_bias[1] * this.mRes * this.asay,
        z: mag_bias[2] * this.mRes * this.asaz
      },
      scale: {
        x: avg_rad / mag_scale[0],
        y: avg_rad / mag_scale[1],
        z: avg_rad / mag_scale[2]
      }
    };
  };

  getFactoryCalibration() {
    if (this.config.debug) { console.time('AK8963 getFactoryCalibration') };
  	// Need to set to Fuse mode to get valid values from this.
  	const currentMode = this.getMode();
  	this.setMode(this.CNTL_MODE_FUSE_ROM_ACCESS);
  	sleep.msleep(10);
  	// Get the ASA* values
  	this.asax = ((this.i2cBus.readByteSync(this.address, this.RA_ASAX) - 128) / 256 + 1);
  	this.asay = ((this.i2cBus.readByteSync(this.address, this.RA_ASAY) - 128) / 256 + 1);
  	this.asaz = ((this.i2cBus.readByteSync(this.address, this.RA_ASAZ) - 128) / 256 + 1);
  	// Return the mode we were in before
  	this.setMode(currentMode);
    sleep.msleep(10);
    if (this.config.debug) { console.time('AK8963 getFactoryCalibration') };
  };

  readRawMagnetometer() {
  	// Get the actual data
    const buf = Buffer.alloc(6);
    this.i2cBus.readI2cBlockSync(this.address, this.RA_XOUT_L, 6, buf);
    // Confirm read finished by reading ST2 register. AK8963 datasheet p.15
    const st2 = this.i2cBus.readByteSync(this.address, this.RA_ST2);
    // Check if sensor overflow occurred
    const overflow = I2CBUS.readBit(st2, this.ST2_HOFL_BIT);
    if (overflow === 1) {
      return [0, 0, 0];
    }
    return [
      buf.readInt16LE(0),
      buf.readInt16LE(2),
      buf.readInt16LE(4)
  	];
  };

  getDataReady() {
  	if (this.i2cBus) {
      const byte = this.i2cBus.readByteSync(this.address, this.RA_ST1);
      return I2CBUS.readBit(byte, this.ST1_DRDY_BIT);
  	}
  	return false;
  };

  getMode() {
  	if (this.i2cBus) {
  		return this.i2cBus.readByteSync(this.address, this.RA_CNTL) & 0b1111;
  	}
  	return false;
  };

  getResolution() {
    const byte = this.i2cBus.readByteSync(this.address, this.RA_CNTL);
    return I2CBUS.readBit(byte, 4);
  };

  getDeviceID() {
  	if (this.i2cBus) {
  		return this.i2cBus.readByteSync(this.address, this.RA_WHO_AM_I);
  	}
  };

  setMode(mode) {
    this.logger.debug(`Setting mode to 0b${mode.toString(2)} ...`);
    if (mode === this.CNTL_MODE_OFF ||
        mode === this.CNTL_MODE_SINGLE_MEASURE ||
        mode === this.CNTL_MODE_CONT_8HZ ||
        mode === this.CNTL_MODE_CONT_100HZ ||
        mode === this.CNTL_MODE_EXT_TRIG_MEASURE ||
        mode === this.CNTL_MODE_SELF_TEST_MODE ||
        mode === this.CNTL_MODE_FUSE_ROM_ACCESS) {
      if (this.i2cBus) {
        this.i2cBus.writeBits(this.address, this.RA_CNTL, 0, 4, mode);
    	}
    }
  	return false;
  };

  setResolution(value) {
    this.logger.debug(`Setting resolution to 0x${value.toString(16)} ...`);
    if (!Number.isInteger(value) || value < 0 || value > 1) {
      throw new Error('Magnetometer resolution value must be an int between [0,1]');
    }
    this.i2cBus.writeBits(this.address, this.RA_CNTL, 4, 1, value);
    sleep.msleep(10);
    this.mRes = this.MRESOLUTIONS[value];
  };

  printSettings() {
  	const MODE_LST = {
  		0: '0x00 (Power-down mode)',
  		1: '0x01 (Single measurement mode)',
  		2: '0x02 (Continuous measurement mode 1: 8Hz)',
  		6: '0x06 (Continuous measurement mode 2: 100Hz)',
  		4: '0x04 (External trigger measurement mode)',
  		8: '0x08 (Self-test mode)',
  		15: '0x0F (Fuse ROM access mode)'
  	};
    const RES_LIST = {
      0: '14 bits',
      1: '16 bits'
    };
  	this.logger.info(`INFO Magnetometer (Compass):`);
  	this.logger.info(`INFO --> i2c address: 0x${this.address.toString(16)}`);
  	this.logger.info(`INFO --> Device ID: 0x${this.getDeviceID().toString(16)}`);
  	this.logger.info(`INFO --> Mode: ${MODE_LST[this.getMode()]}`);
    this.logger.info(`INFO --> Resolution: ${RES_LIST[this.getResolution()]}`);
    this.logger.info(`INFO --> Factory calibration:`);
    this.logger.info(`INFO   --> x: ${this.asax}`);
    this.logger.info(`INFO   --> y: ${this.asay}`);
    this.logger.info(`INFO   --> z: ${this.asaz}`);
  };

};

module.exports = AK8963;
